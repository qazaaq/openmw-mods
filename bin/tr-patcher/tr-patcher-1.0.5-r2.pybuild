# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import stat
import sys

from pybuild import Pybuild2, patch_dir
from pybuild.info import PN


class Package(Pybuild2):
    NAME = "Tamriel Rebuilt Filepatcher (CLI version)"
    DESC = "A modified version of the tr-patcher by the Tamriel Rebuilt Team with a CLI"
    HOMEPAGE = "https://gitlab.com/bmwinger/tr-patcher"
    SRC_URI = """
        https://gitlab.com/bmwinger/tr-patcher/uploads/d014a589fc4c12f6ac77f531f392e32e/tr-patcher.zip
    """
    LICENSE = "GPL-3"
    KEYWORDS = "openmw"
    RDEPEND = "sys-bin/java"
    S = f"{PN}/{PN}"

    def src_install(self):
        patch_dir(
            os.path.join(self.WORKDIR, self.S, "bin"), os.path.join(self.D, "bin")
        )
        exe_name = os.path.join(
            self.D, "bin", "tr-patcher.bat" if sys.platform == "win32" else "tr-patcher"
        )
        # Add executable permissions (may not be preserved when extracting from zip)
        os.chmod(
            exe_name,
            os.stat(exe_name).st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH,
        )

        patch_dir(
            os.path.join(self.WORKDIR, self.S, "lib"),
            os.path.join(self.D, "lib"),
        )
