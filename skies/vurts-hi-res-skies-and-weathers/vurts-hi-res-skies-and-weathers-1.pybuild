# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import re

from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Vurts Hi-res Skies and Weathers"
    DESC = "Hi-res Skies and weather Pack for use with the marvelous Skies .IV mod."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/28552"
    LICENSE = "all-rights-reserved"
    RDEPEND = "skies/swg-skies[night-sky-mesh,sky-mesh]"
    KEYWORDS = "openmw"
    SRC_URI = "HiRes_Skies_and_Weathers-28552.rar"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/28552"
    TEXTURE_SIZES = "4096"
    IUSE = "alt-night-sky light-settings"
    INSTALL_DIRS = [
        InstallDir(".", RENAME="textures"),
        InstallDir(
            "Night Sky alternative version",
            RENAME="textures",
            REQUIRED_USE="alt-night-sky",
        ),
    ]

    def src_prepare(self):
        # Extract ini changes from readme.txt
        # This primarily includes several weather sections,
        # as well as an optional LightAttentuation section
        section = None
        self.FALLBACK = {}
        begin = False
        with open("readme.txt", "r") as config:
            for line in config.readlines():
                if (
                    not begin
                    and "[Weather]" not in line
                    and "[LightAttenuation]" not in line
                ):
                    continue
                elif "[Weather]" in line:
                    begin = True
                elif "[LightAttenuation]" in line and "light-settings" in self.USE:
                    begin = True
                elif "--------" in line:
                    begin = False

                if begin:
                    if re.match(r"\s*^\[.*\]\s*$", line):
                        section = line.strip().strip("[]")
                        self.FALLBACK[section] = {}
                    elif "=" in line and not line.startswith(";"):
                        key = line.split("=", 1)[0].strip()
                        value = line.split("=", 1)[1].strip()
                        self.FALLBACK[section][key] = value
